package com.glasstowerstudios.rucksack.ui.fragment;

import android.app.Activity;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.glasstowerstudios.rucksack.R;
import com.glasstowerstudios.rucksack.di.Injector;
import com.glasstowerstudios.rucksack.model.Trip;
import com.glasstowerstudios.rucksack.ui.activity.BaseActivity;
import com.glasstowerstudios.rucksack.ui.activity.TripsActivity;
import com.glasstowerstudios.rucksack.ui.adapter.TripRecyclerAdapter;
import com.glasstowerstudios.rucksack.ui.base.DividerItemDecoration;
import com.glasstowerstudios.rucksack.ui.observer.TripSelectionListener;
import com.glasstowerstudios.rucksack.util.data.TripDataProvider;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

import static android.support.v7.widget.RecyclerView.AdapterDataObserver;
import static android.support.v7.widget.RecyclerView.ItemDecoration;
import static android.support.v7.widget.RecyclerView.LayoutManager;
import static android.support.v7.widget.RecyclerView.State;
import static android.support.v7.widget.RecyclerView.ViewHolder;

/**
 * A listing of trips within the system.
 */
public class TripRecyclerFragment
  extends Fragment
  implements SwipeRefreshLayout.OnRefreshListener, TripSelectionListener {

  private static final String LOGTAG = TripRecyclerFragment.class.getSimpleName();

  @Bind(R.id.trip_recycler_view)
  protected RecyclerView mRecyclerView;

  private TripRecyclerAdapter mAdapter;

  @Bind(R.id.trip_empty_view)
  protected View mEmptyView;

  @Bind(R.id.trips_swipe_refresh)
  protected SwipeRefreshLayout mSwipeRefreshLayout;

  @Inject TripDataProvider mTripDataProvider;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    Injector.INSTANCE.getApplicationComponent().inject(this);
  }

  private void setUpRecyclerView() {
    LayoutManager layoutManager = new LinearLayoutManager(getActivity());
    mRecyclerView.setLayoutManager(layoutManager);
    mAdapter = new TripRecyclerAdapter(getTrips());
    mAdapter.addTripSelectionListener(this);
    mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
    mRecyclerView.setAdapter(mAdapter);
    mRecyclerView.setHasFixedSize(true);
    mAdapter.registerAdapterDataObserver(new AdapterDataObserver() {
      @Override
      public void onItemRangeRemoved(int positionStart, int itemCount) {
        super.onItemRangeRemoved(positionStart, itemCount);
        refreshTripVisibility();
      }
    });

    setUpItemTouchHelper();
    setUpAnimationDecoratorHelper();
  }

  /**
   * Set up our item touch helper so we can enable "swipe to delete".
   *
   * This pattern was taken from https://github.com/nemanja-kovacevic/recycler-view-swipe-to-delete
   *
   * This is the standard support library way of implementing "swipe to delete" feature. You can do
   * custom drawing in the onChildDraw method, but whatever you draw will disappear once the swipe
   * is over, and while the items are animating to their new position, the recycler view background
   * will be visible. This is rarely a desired effect.
   */
  private void setUpItemTouchHelper() {

    ItemTouchHelper.SimpleCallback simpleItemTouchCallback =
      new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
      // We want to cache these and not allocate anything repeatedly in the onChildDraw method
      Drawable background;
      Drawable xMark;
      int xMarkMargin;
      boolean initiated;

      private void init() {
        background = new ColorDrawable(getResources().getColor(R.color.dove_gray));
        xMark = ContextCompat.getDrawable(getContext(), R.drawable.ic_action_delete);
        xMark.setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_ATOP);
        xMarkMargin = (int) getResources().getDimension(R.dimen.icon_margin);
        initiated = true;
      }

      // This is only used for drag and drop, so we don't really need it.
      @Override
      public boolean onMove(RecyclerView recyclerView, ViewHolder viewHolder, ViewHolder target) {
        return false;
      }

      @Override
      public int getSwipeDirs(RecyclerView recyclerView, ViewHolder viewHolder) {
        int position = viewHolder.getAdapterPosition();
        TripRecyclerAdapter adapter = (TripRecyclerAdapter) recyclerView.getAdapter();
        if (adapter.isUndoOn() && adapter.isPendingRemoval(position)) {
          return 0;
        }

        return super.getSwipeDirs(recyclerView, viewHolder);
      }

      @Override
      public void onSwiped(ViewHolder viewHolder, int swipeDir) {
        int swipedPosition = viewHolder.getAdapterPosition();
        TripRecyclerAdapter adapter = (TripRecyclerAdapter) mRecyclerView.getAdapter();
        boolean undoOn = adapter.isUndoOn();

        if (undoOn) {
          adapter.setItemPendingRemoval(swipedPosition);
        } else {
          adapter.remove(swipedPosition);
        }
      }

      @Override
      public void onChildDraw(Canvas c, RecyclerView recyclerView,
                              ViewHolder viewHolder, float dX, float dY,
                              int actionState, boolean isCurrentlyActive) {
        View itemView = viewHolder.itemView;

        // Not sure why, but this method is called for viewholder objects that are already swiped
        // away
        if (viewHolder.getAdapterPosition() == -1) {
          // We're not interested in these
          return;
        }

        if (!initiated) {
          init();
        }

        background.setBounds(itemView.getRight() + (int) dX, itemView.getTop(), itemView.getRight(),
                             itemView.getBottom());
        background.draw(c);

        // Draw x mark to indicate to user starting to swipe the action that will happen.
        int itemHeight = itemView.getBottom() - itemView.getTop();
        int intrinsicWidth = xMark.getIntrinsicWidth();
        int intrinsicHeight = xMark.getIntrinsicWidth();

        int xMarkLeft = itemView.getRight() - xMarkMargin - intrinsicWidth;
        int xMarkRight = itemView.getRight() - xMarkMargin;
        int xMarkTop = itemView.getTop() + (itemHeight - intrinsicHeight)/2;
        int xMarkBottom = xMarkTop + intrinsicHeight;
        xMark.setBounds(xMarkLeft, xMarkTop, xMarkRight, xMarkBottom);

        xMark.draw(c);

        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
      }
    };

    ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
    mItemTouchHelper.attachToRecyclerView(mRecyclerView);
  }

  /**
   * Set up the animations that take effect after a swipe has occurred.
   *
   * This pattern taken from https://github.com/nemanja-kovacevic/recycler-view-swipe-to-delete
   *
   * We're gonna setup another ItemDecorator that will draw the background in the empty space while
   * the items are animating to their new positions. This will happen after an item is removed.
   */
  private void setUpAnimationDecoratorHelper() {
    mRecyclerView.addItemDecoration(new ItemDecoration() {

      // we want to cache this and not allocate anything repeatedly in the onDraw method
      Drawable background;
      boolean initiated;

      private void init() {
        background = new ColorDrawable(getResources().getColor(R.color.dove_gray));
        initiated = true;
      }

      @Override
      public void onDraw(Canvas c, RecyclerView parent, State state) {

        if (!initiated) {
          init();
        }

        // only if animation is in progress
        if (parent.getItemAnimator().isRunning()) {

          // some items might be animating down and some items might be animating up to close the
          // gap left by the removed item. This is not exclusive - both movements can be happening
          // at the same time. To reproduce this, leave just enough items so the first one and the
          // last one would be just a little off screen, then remove one from the middle.

          // Find first child with translationY > 0
          // and last one with translationY < 0
          // we're after a rect that is not covered in recycler-view views at this point in time
          View lastViewComingDown = null;
          View firstViewComingUp = null;

          // this is fixed
          int left = 0;
          int right = parent.getWidth();

          // this we need to find out
          int top = 0;
          int bottom = 0;

          // find relevant translating views
          int childCount = parent.getLayoutManager().getChildCount();
          for (int i = 0; i < childCount; i++) {
            View child = parent.getLayoutManager().getChildAt(i);
            if (child.getTranslationY() < 0) {
              // view is coming down
              lastViewComingDown = child;
            } else if (child.getTranslationY() > 0) {
              // view is coming up
              if (firstViewComingUp == null) {
                firstViewComingUp = child;
              }
            }
          }

          if (lastViewComingDown != null && firstViewComingUp != null) {
            // views are coming down AND going up to fill the void
            top = lastViewComingDown.getBottom() + (int) lastViewComingDown.getTranslationY();
            bottom = firstViewComingUp.getTop() + (int) firstViewComingUp.getTranslationY();
          } else if (lastViewComingDown != null) {
            // views are going down to fill the void
            top = lastViewComingDown.getBottom() + (int) lastViewComingDown.getTranslationY();
            bottom = lastViewComingDown.getBottom();
          } else if (firstViewComingUp != null) {
            // views are coming up to fill the void
            top = firstViewComingUp.getTop();
            bottom = firstViewComingUp.getTop() + (int) firstViewComingUp.getTranslationY();
          }

          background.setBounds(left, top, right, bottom);
          background.draw(c);

        }
        super.onDraw(c, parent, state);
      }

    });
  }

  @Override
  public void onResume() {
    super.onResume();
    Activity act = getActivity();
    BaseActivity baseAct = (BaseActivity) act;
    baseAct.unlockNavigationDrawer();

    onRefresh();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View createdView = inflater.inflate(R.layout.fragment_trip_recycler, container, false);
    ButterKnife.bind(this, createdView);

    TripsActivity act = (TripsActivity) getContext();
    act.enableFloatingActionButton();

    ActionBar appBar = act.getSupportActionBar();
    if (appBar != null) {
      appBar.setTitle(R.string.trips);
    }

    setUpRecyclerView();

    mSwipeRefreshLayout.setOnRefreshListener(this);

    return createdView;
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
    mAdapter.removeTripSelectionListener(this);
  }

  private List<Trip> getTrips() {
    List<Trip> trips = mTripDataProvider.getAll();
    return trips;
  }

  @Override
  public void onRefresh() {
    mSwipeRefreshLayout.setRefreshing(true);

    List<Trip> trips = getTrips();
    mAdapter.setTrips(trips);

    refreshTripVisibility();

    mSwipeRefreshLayout.setRefreshing(false);
  }

  private void refreshTripVisibility() {
    if (mAdapter.getItemCount() > 0) {
      mRecyclerView.setVisibility(View.VISIBLE);
      mEmptyView.setVisibility(View.GONE);
    } else {
      mRecyclerView.setVisibility(View.GONE);
      mEmptyView.setVisibility(View.VISIBLE);
    }

  }

  @Override
  public void onTripSelected(Trip aTrip) {
    BaseActivity baseAct = (BaseActivity) getActivity();

    Bundle args = new Bundle();
    args.putParcelable(TripInteractionFragment.TRIP_KEY, aTrip);

    baseAct.showFragment(TripInteractionFragment.class, args, true);
  }
}
