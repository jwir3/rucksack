package com.glasstowerstudios.rucksack.ui.adapter;

import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.glasstowerstudios.rucksack.R;
import com.glasstowerstudios.rucksack.di.Injector;
import com.glasstowerstudios.rucksack.model.Trip;
import com.glasstowerstudios.rucksack.ui.observer.TripSelectionListener;
import com.glasstowerstudios.rucksack.util.TemporalFormatter;
import com.glasstowerstudios.rucksack.util.data.TripDataProvider;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * A {@link RecyclerView.Adapter} that adapts {@link Trip} objects to be presented in a
 * {@link RecyclerView}. Typically used by the {@link com.glasstowerstudios.rucksack.ui.fragment.TripRecyclerFragment}.
 */
public class TripRecyclerAdapter extends RecyclerView.Adapter<TripRecyclerAdapter.TripViewHolder> {
  private static final String LOGTAG = TripRecyclerAdapter.class.getSimpleName();
  private static final int PENDING_REMOVAL_TIMEOUT = 5000; // 5 seconds

  public static class TripViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.trip_destination_name) protected TextView mDestinationName;
    @Bind(R.id.trip_date_range) protected TextView mTripDateRange;
    @Bind(R.id.trip_pending_removal_view) protected View pendingRemovalView;
    @Bind(R.id.trip_list_data) protected View dataView;
    @Bind(R.id.trip_undo_removal_button) protected Button undoButton;

    public TripViewHolder(View view) {
      super(view);
      ButterKnife.bind(this, view);
    }
  }

  @Inject TripDataProvider mTripDataProvider;

  private List<Trip> mTrips;
  private List<Trip> itemsPendingRemoval;

  private List<TripSelectionListener> mListeners = new LinkedList<>();
  private boolean undoOn = true;

  private Handler handler = new Handler(); // Handler for running delayed runnables
  private HashMap<Trip, Runnable> pendingRunnables = new HashMap<>(); // map of items to pending runnables, so we can cancel a removal if need be

  public TripRecyclerAdapter(List<Trip> trips) {
    mTrips = trips;
    itemsPendingRemoval = new ArrayList<>();
    Injector.INSTANCE.getApplicationComponent().inject(this);
  }

  // Create new views (invoked by the layout manager)
  @Override
  public TripRecyclerAdapter.TripViewHolder onCreateViewHolder(ViewGroup parent,
                                                               int viewType) {
    // create a new view
    View v = LayoutInflater.from(parent.getContext())
                           .inflate(R.layout.trip_list_item, parent, false);

    TripViewHolder vh = new TripViewHolder(v);

    return vh;
  }

  // Replace the contents of a view (invoked by the layout manager)
  @Override
  public void onBindViewHolder(TripRecyclerAdapter.TripViewHolder holder, int position) {
    Trip item = mTrips.get(position);
    if (itemsPendingRemoval.contains(item)) {
      // we need to show the "undo" state of the row
      holder.pendingRemovalView.setVisibility(View.VISIBLE);
      holder.dataView.setVisibility(View.GONE);
      holder.undoButton.setVisibility(View.VISIBLE);
      holder.undoButton.setOnClickListener(v -> {
        // user wants to undo the removal, let's cancel the pending task
        Runnable pendingRemovalRunnable = pendingRunnables.get(item);
        pendingRunnables.remove(item);
        if (pendingRemovalRunnable != null) handler.removeCallbacks(pendingRemovalRunnable);
        itemsPendingRemoval.remove(item);
        // this will rebind the row in "normal" state
        notifyItemChanged(mTrips.indexOf(item));
      });
    } else {
      Trip boundTrip = mTrips.get(position);
      holder.mDestinationName.setText(boundTrip.getDestinationName());

      DateTime startDate = boundTrip.getStartDate();
      DateTime endDate = boundTrip.getEndDate();

      DateTimeFormatter formatter = TemporalFormatter.TRIP_DATES_FORMATTER;
      String dateRangeTemplate =
        String.format(holder.mTripDateRange.getResources().getString(R.string.trip_date_range),
                      formatter.print(startDate),
                      formatter.print(endDate));
      holder.mTripDateRange.setText(dateRangeTemplate);

      holder.itemView.setOnClickListener(view -> {
        notifyTripSelectionListeners(boundTrip);
      });
    }
  }

  @Override
  public int getItemCount() {
    return mTrips.size();
  }

  public void add(Trip trip) {
    add(trip, mTrips.size());
  }

  public void add(Trip trip, int position) {
    mTrips.add(position, trip);
    notifyDataSetChanged();
  }

  public void setTrips(List<Trip> trips) {
    mTrips = trips;
    notifyDataSetChanged();
  }

  public void remove(int position) {
    // It's possible that our position can be < 0 in the event that we removed the pending item(s)
    // via a click. In this case, we'll just ignore the event (since we don't really need to cancel
    // here).
    if (position >= 0) {
      Trip t = mTrips.get(position);
      if (itemsPendingRemoval.contains(t)) {
        itemsPendingRemoval.remove(t);
        mTripDataProvider.delete(t);
      }

      if (mTrips.contains(t)) {
        mTrips.remove(position);
        notifyItemRemoved(position);
      }
    }
  }

  public void clearTripSelectionListeners() {
    mListeners.clear();
  }

  public void addTripSelectionListener(TripSelectionListener listener) {
    mListeners.add(listener);
  }

  public void removeTripSelectionListener(TripSelectionListener listener) {
    mListeners.remove(listener);
  }

  public void setUndoOn(boolean undoOn) {
    this.undoOn = undoOn;
  }

  public boolean isUndoOn() {
    return undoOn;
  }

  public void setItemPendingRemoval(int position) {
    final Trip item = mTrips.get(position);
    if (!itemsPendingRemoval.contains(item)) {
      itemsPendingRemoval.add(item);
      // this will redraw row in "undo" state
      notifyItemChanged(position);
      // let's create, store and post a runnable to remove the item
      Runnable pendingRemovalRunnable = () -> remove(mTrips.indexOf(item));

      handler.postDelayed(pendingRemovalRunnable, PENDING_REMOVAL_TIMEOUT);
      pendingRunnables.put(item, pendingRemovalRunnable);
    }
  }

  public boolean isPendingRemoval(int position) {
    Trip item = mTrips.get(position);
    return itemsPendingRemoval.contains(item);
  }

  private void notifyTripSelectionListeners(Trip selectedTrip) {
    // First, let's make sure we're removing anything that's pending so we don't get caught in a
    // weird state.
    removePendingItems();

    for (TripSelectionListener listener : mListeners) {
      listener.onTripSelected(selectedTrip);
    }
  }

  private void removePendingItems() {
    List<Integer> posList = new LinkedList<>();
    for (Trip nextPendingItem : itemsPendingRemoval) {
      posList.add(mTrips.indexOf(nextPendingItem));
    }

    for (Integer nextIndex : posList) {
      remove(nextIndex);
    }
  }
}

